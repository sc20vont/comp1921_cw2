//
// Created by Vanns Tacey on 17/03/2021.
//

#ifndef CW2_INPUTPROCESSING_H
#define CW2_INPUTPROCESSING_H

extern const char* phraseDB;
extern const char* wordDB;

extern int unsure;

char buffer[1030];
char bufferToUse[1024];

/**
 * Searches the word text file for some of the words inputted
 * <p> if multiple words are found on different lines of the file, the theme with the higher word count is returned </p>
 * @return an integer corresponding to the subject of the input found
 * @author Vanns Tacey
 */
int searchWordDB();


/**
 * Checks if a given input has a vowel
 * <p> used in isWord() </p>
 * @param wordToCheck - word to check from 2D array (userInputSplit)
 * @return integer status code
 * @author Vanns Tacey
 */
int hasVowel(int wordToCheck);


/**
 * Checks if a given input has multiple different letters
 * <p> used in isWord() </p>
 * @param wordToCheck - word to check from 2D array (userInputSplit)
 * @return integer status code
 * @author Vanns Tacey
 */
int hasDifferentLetters(int wordToCheck);


/**
 * Checks if a given input has a consonant
 * <p> used in isWord() </p>
 * @param wordToCheck - word to check from 2D array (userInputSplit)
 * @return integer status code
 * @author Vanns Tacey
 */
int hasConsonant(int wordToCheck);


/**
 * Checks if a given input has any numbers
 * <p> used in isWord() </p>
 * @param wordToCheck - word to check from 2D array (userInputSplit)
 * @return integer status code
 * @author Vanns Tacey
 */
int hasNoNumbers(int wordToCheck);

/**
 * Checks if a given input is *likely* to be a word
 * @param wordToCheck - word to check from 2D array (userInputSplit)
 * @return integer status code
 * @author Vanns Tacey
 */
int isWord(int wordToCheck);




#endif //CW2_INPUTPROCESSING_H
