//
// Created by Vanns Tacey on 21/03/2021.
//

#ifndef CW2_INTERFACE_H
#define CW2_INTERFACE_H

/**
 * Prints the opening text to welcome the user
 * @author Vanns Tacey
 */
void welcomeUser();

/**
 * Collects the user's input from the input stream
 * <p> Sets global variables to be used throughout program based on some
 * boolean variables set during program operation.</p>
 * @author Vanns Tacey
 */
void getUserInput();

/**
 * Asks the user if they want the program to learn their previous input
 * <p> Loops indefinitely until a reasonable response is submitted</p>
 * @return integer status code based on the response by the user
 * @author Vanns Tacey
 */
int learnInput();

/**
 * Runs the word checks to make sure input is reasonable
 * @return integer status code based on the input it is checking
 * @author Vanns Tacey
 */
int checkInput();

/**
 * Asks the user if the output given by the program is reasonable
 * <p> Loops indefinitely until a reasonable response is submitted</p>
 * @return integer status code based on the response by the user
 * @author Vanns Tacey
 */
int getApprovalFromUser();

/**
 * The program loop that runs the process of:
 *  <p>- Getting the user's input</p>
 *  <p>- Processing that input and working out the meaning</p>
 *  <p>- Outputting a response</p>
 * <p> Loops indefinitely until the conversation is ended by the user or by the program</p>
 * @return integer status code based on processing of the input
 * @author Vanns Tacey
 */
int programLoop();

#endif //CW2_INTERFACE_H
