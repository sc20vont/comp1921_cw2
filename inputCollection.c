//
// Created by Vanns Tacey on 15/03/2021.
//

#include "inputCollection.h"
#include "global.h"
#include <stdio.h>
#include <string.h>

const char* startUpGreeting = "Welcome to Vanns Tacey's C Chatbot!";
const char* promptUser = "How are you today?";


int splitInputtedString(int toLearn) {
    int counter = 0;
    int index = 0;
    char *stringToSplit;
    char (*ptrTo2DArray)[45];

//    Determines what string to manipulate
    if (toLearn == 1) {
        stringToSplit = strdup(userInputToLearn);
        ptrTo2DArray = userInputToLearnSplit;
    } else {
        stringToSplit = strdup(userInput);
        ptrTo2DArray = userInputSplit;
    }

//    Loop through entire string
    for (int i = 0; i <= strlen(stringToSplit); i++) {
//        If character is the end of string or a space, end the current entry into array
        if (stringToSplit[i] == ' ' || stringToSplit[i] == '\0') {
            *(*(ptrTo2DArray + counter) + index) = '\0';
            counter++;
            index = 0;
//            Otherwise carry on adding characters to array entry
        } else {
            *(*(ptrTo2DArray + counter) + index) = stringToSplit[i];
            index++;
        }
    }

    return counter;
}


const char* collectUserInput() {
//  If input is longer, a default response will be used
    char inputString[1024];
//  Loops until input is received
    while (fgets(inputString, sizeof(inputString), stdin) != NULL) {
        if (strcmp(inputString, "\n") != 0) {
            inputString[strlen(inputString)-1] = '\0';
            break;
        }
    }
//  Duplicate the string so value is not lost
    return strdup(inputString);
}


