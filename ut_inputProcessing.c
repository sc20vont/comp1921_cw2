#include "Unity_ToCopy/src/unity.h"
#include "inputProcessing.h"
#include "global.h"

#include <string.h>

char userInput[1024];
int lenOfUserInput = 0;
char userInputSplit[1024][45];
int numberOfWords = 0;

char userInputToLearn[1024];
int lenOfUserInputToLearn = 0;
char userInputToLearnSplit[1024][45];
int numberOfWords_l = 0;
char userInputPipes[1024];
int maxSubject = 13;


int rtn_addToDBs;
int noOfLines_word = 0;
int noOfLines_phrase = 0;

/**
 * Counts the number of lines in a given file
 * @param f - the file to be used for counting
 * @return amount of lines in file
 * @author Vanns Tacey
 */
int getLineCount(FILE *f) {
    int rtnVal = 0;
    char tmp_buffer[1024];

    while(fgets(tmp_buffer, sizeof(tmp_buffer) ,f) != NULL) {
        rtnVal++;
    }

    fclose(f);
    return rtnVal;
}


void setUp() {}


void tearDown() {}

/**
 * Checks return value is not null
 */
void test_addToDBs1() {
    TEST_ASSERT_NOT_NULL_MESSAGE(rtn_addToDBs, "Returned null value");
}

/**
 * Checks return value is within defined range
 */
void test_addToDBs2() {
    int bool = 0;
    if (rtn_addToDBs >= -2 && rtn_addToDBs <= 1) {
       bool = 1;
    }

    TEST_ASSERT(bool);
}

/**
 * Checks files have increased in size with the new entry
 * <p> DELETE LAST LINE OF "txtFiles/phrase_db.txt" AND "txtFiles/word_db.txt" BEFORE RUNNING MAIN PROGRAM </p>
 */
void test_addToDBs3() {
    int bool = 0;

    int test_noOfLines_word = 0;
    int test_noOfLines_phrase = 0;

    FILE *test_f_word = fopen(wordDB,"r");
    test_noOfLines_word = getLineCount(test_f_word);

    FILE *test_f_phrase = fopen(phraseDB,"r");
    test_noOfLines_phrase = getLineCount(test_f_phrase);

    if ((test_noOfLines_word == noOfLines_word+1) && (test_noOfLines_phrase == noOfLines_phrase+1)) {
        bool = 1;
    }

    TEST_ASSERT(bool);
}

/**
 * Checks cleanString() returns the subject type
 */
void test_cleanString1() {
    FILE *f = fopen(wordDB, "r");

    fgets(buffer, sizeof(buffer) ,f);
    int test_cleanString_rtn = cleanString();

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_cleanString_rtn, "Returned value did not match data in file");
}

/**
 * Checks cleanString() successfully removes the subject type and the '|' from the string
 */
void test_cleanString2() {
    TEST_ASSERT_EQUAL_STRING_MESSAGE("hello", bufferToUse, "String was not 'cleaned' correctly");
}

/**
 * Checks hasVowel() successfully find a vowel in a given word
 */
void test_hasVowel() {
    int test_hasVowel_rtn = hasVowel(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_hasVowel_rtn, "Did not find a vowel in 'This'");
}

/**
 * Checks hasConsonant() successfully find a consonant in a given word
 */
void test_hasConsonant() {
    int test_hasConsonant_rtn = hasConsonant(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_hasConsonant_rtn, "Did not find a consonant in 'This'");
}

/**
 * Checks hasNoNumbers() successfully finds no numbers in a given word
 */
void test_hasNoNumbers() {
    int test_hasNoNumbers_rtn = hasNoNumbers(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_hasNoNumbers_rtn, "Found numbers in 'This'");
}

/**
 * Checks hasDifferentLetters() successfully finds no simultaneous letters in a given word
 */
void test_hasDifferentLetters() {
    int test_hasDifferentLetters_rtn = hasDifferentLetters(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_hasDifferentLetters_rtn, "Found too many simultaneous letters in 'This'");
}

/**
 * Checks isWord() successfully finds a word to be valid by using hasVowel(), hasConsonant(), hasNoNumbers(), hasDifferentLetters()
 */
void test_isWord1() {
    int test_isWord_rtn = isWord(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(1, test_isWord_rtn, "'This' was found to not be a valid word");
}

/**
 * Checks isWord() successfully finds an invalid word to be invalid by using hasVowel(), hasConsonant(), hasNoNumbers(), hasDifferentLetters()
 */
void test_isWord2() {
    int test_isWord_rtn = isWord(4);

    TEST_ASSERT_EQUAL_INT_MESSAGE(0, test_isWord_rtn, "'c99' was found to be a valid word");
}

/**
 * Checks searchPhraseDB() successfully returns 0 when a phrase isn't in phraseDB.txt
 */
void test_searchPhraseDB1() {
    int test_searchPhraseDB_rtn = searchPhraseDB(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(0, test_searchPhraseDB_rtn, "'This is a test' was found in the PhraseDB");
}

/**
 * Checks searchPhraseDB() successfully finds a phrase in phraseDB.txt
 */
void test_searchPhraseDB2() {
    strcpy(userInput, "i read a book");
    int test_searchPhraseDB_rtn = searchPhraseDB(0);

    TEST_ASSERT_EQUAL_INT_MESSAGE(5, test_searchPhraseDB_rtn, "'i read a book' was not found in the PhraseDB");
}


/**
 * Checks searchWordDB() successfully finds a phrase that is similar to one in wordDB.txt and returns the subject type
 */
void test_searchWordDB1() {
    strcpy(userInputSplit[0], "i");
    strcpy(userInputSplit[1], "read");
    strcpy(userInputSplit[2], "a");
    strcpy(userInputSplit[3], "comic");
    int test_searchWordDB_rtn = searchWordDB();

    TEST_ASSERT_EQUAL_INT_MESSAGE(5,  test_searchWordDB_rtn, "'i read a comic' was not found to be similar to anything in the wordDB");
}

/**
 * Checks searchWordDB() successfully returns 0 when a word is not found in any of the records in wordDB.txt
 */
void test_searchWordDB2() {
    strcpy(userInputSplit[0], "agh");
    numberOfWords = 1;
    int test_searchWordDB_rtn = searchWordDB();

    TEST_ASSERT_EQUAL_INT_MESSAGE(0, test_searchWordDB_rtn, "'agh' was found to be in wordDB.txt");
}

/**
 * Checks searchWordDB() successfully returns a subject type when a word is similar to multiple records in wordDB.txt
 */
void test_searchWordDB3() {
    strcpy(userInputSplit[0], "i");
    strcpy(userInputSplit[1], "saw");
    strcpy(userInputSplit[2], "my");
    strcpy(userInputSplit[3], "steven");
    numberOfWords = 4;
    int test_searchWordDB_rtn = searchWordDB();

    TEST_ASSERT_EQUAL_INT_MESSAGE(8, test_searchWordDB_rtn, "'i saw my steven' caused some confusion when searching in wordDB.txt");
}

/**
 * Checks searchWordDB() successfully returns 0 when a phrase is close to matching multiple records with different subject types in wordDB.txt
 */
void test_searchWordDB4() {
    strcpy(userInputSplit[0], "you");
    strcpy(userInputSplit[1], "are");
    strcpy(userInputSplit[2], "abnormal");
    numberOfWords = 3;
    int test_searchWordDB_rtn = searchWordDB();

    TEST_ASSERT_EQUAL_INT_MESSAGE(0, test_searchWordDB_rtn, "'you are abnormal' did not cause any confusion when searching in wordDB.txt");
}

int main() {
    UNITY_BEGIN();
    strcpy(userInputToLearn, "This is a test");
    strcpy(userInput, "This is a test");

    strcpy(userInputToLearnSplit[0], "This");
    strcpy(userInputToLearnSplit[1], "is");
    strcpy(userInputToLearnSplit[2], "a");
    strcpy(userInputToLearnSplit[3], "test");
    strcpy(userInputToLearnSplit[4], "c99");

    strcpy(userInputSplit[0], "This");
    strcpy(userInputSplit[1], "is");
    strcpy(userInputSplit[2], "a");
    strcpy(userInputSplit[3], "test");
    strcpy(userInputToLearnSplit[4], "c99");


    numberOfWords_l = 4;
    numberOfWords = 4;
    lenOfUserInputToLearn = 4;

/**
 * Uncomment the following lines to test addToDBs
 * THIS WILL WRITE LINES IN THE DATABASE THAT NEED TO BE DELETED BEFORE RUNNING MAIN PROGRAM
 */
//    FILE *f_word = fopen(wordDB,"r");
//    noOfLines_word = getLineCount(f_word);
//
//    FILE *f_phrase = fopen(phraseDB,"r");
//    noOfLines_phrase = getLineCount(f_phrase);
//
//    rtn_addToDBs = addToDBs(1);
//    RUN_TEST(test_addToDBs1);
//    RUN_TEST(test_addToDBs2);
//    RUN_TEST(test_addToDBs3);
    RUN_TEST(test_cleanString1);
    RUN_TEST(test_cleanString2);
    RUN_TEST(test_hasVowel);
    RUN_TEST(test_hasConsonant);
    RUN_TEST(test_hasDifferentLetters);
    RUN_TEST(test_hasNoNumbers);
    RUN_TEST(test_isWord1);
    RUN_TEST(test_isWord2);
    RUN_TEST(test_searchPhraseDB1);
    RUN_TEST(test_searchPhraseDB2);
    RUN_TEST(test_searchWordDB1);
    RUN_TEST(test_searchWordDB2);
    RUN_TEST(test_searchWordDB3);
    RUN_TEST(test_searchWordDB4);

    return UNITY_END();
}
