//
// Created by Vanns Tacey on 15/03/2021.
//

#ifndef CW2_INPUTCOLLECTION_H
#define CW2_INPUTCOLLECTION_H

extern const char* startUpGreeting;
extern const char* promptUser;


/**
 * Collects user input
 * <p> Loops infinitely until user enters some input </p>
 * @return a char* to the user string
 * @author Vanns Tacey
 */
const char* collectUserInput();


/**
 * Splits the global string of the user's input into individual words
 * @param toLearn - an integer determining what variables to use for splitting
 * @return an integer of the amount of words found
 * @author Vanns Tacey
 * <p> Resources used: https://www.includehelp.com/c-programs/c-program-to-split-string-by-space-into-words.aspx, https://overiq.com/c-programming-101/pointers-and-2-d-arrays/ </p>
 */
int splitInputtedString(int toLearn);

#endif //CW2_INPUTCOLLECTION_H
