#include "Unity_ToCopy/src/unity.h"
#include "inputCollection.h"

#include <string.h>

char userInput[1024];
char userInputSplit[1024][45];

char userInputToLearn[1024];
char userInputToLearnSplit[1024][45];

void setUp() {
    strcpy(userInput, "This is a test");
}

void tearDown() {}

void test_collectUserInput1() {
    const char* rtn_collectUserInput = collectUserInput();
    TEST_ASSERT_NOT_NULL_MESSAGE(rtn_collectUserInput, "Returned null value");
}


void test_collectUserInput2() {
    char* test_string = "This is a test";
    const char* rtn_collectUserInput = collectUserInput();
    TEST_ASSERT_EQUAL_STRING_MESSAGE(test_string, rtn_collectUserInput, "String inputted does not equal test_string");
}


void test_splitInputtedString1() {
    int test_counter = 4;
    int rtn_splitInputtedString = splitInputtedString(0);
    TEST_ASSERT_EQUAL_INT_MESSAGE(test_counter, rtn_splitInputtedString, "Counter does not equal 4");
}


void test_splitInputtedString2() {
    int rtn_splitInputtedString = splitInputtedString(0);
    TEST_ASSERT_NOT_NULL_MESSAGE(rtn_splitInputtedString, "Returned null value");
}


void test_splitInputtedString3() {
    char* test_string_1 = "This";
    char* test_string_2 = "is";
    char* test_string_3 = "a";
    char* test_string_4 = "test";
    splitInputtedString(0);
    TEST_ASSERT_EQUAL_STRING_MESSAGE(test_string_1, userInputSplit[0], "String inputted does not equal test_string_1");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(test_string_2, userInputSplit[1], "String inputted does not equal test_string_2");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(test_string_3, userInputSplit[2], "String inputted does not equal test_string_3");
    TEST_ASSERT_EQUAL_STRING_MESSAGE(test_string_4, userInputSplit[3], "String inputted does not equal test_string_4");
}

int main() {
    UNITY_BEGIN();
    RUN_TEST(test_collectUserInput1);
    RUN_TEST(test_collectUserInput2);
    RUN_TEST(test_splitInputtedString1);
    RUN_TEST(test_splitInputtedString2);
    RUN_TEST(test_splitInputtedString3);

    return UNITY_END();
}
