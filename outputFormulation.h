//
// Created by Vanns Tacey on 17/03/2021.
//

#ifndef CW2_OUTPUTFORMULATION_H
#define CW2_OUTPUTFORMULATION_H

#include <stdio.h>


/**
 * Generates a random double between 0 and 1
 * @return a random double value
 * @author Vanns Tacey
 * <p> Resources used: https://ubuntuforums.org/showthread.php?t=1717717&p=10618266 </p>
 */
double randDouble();

/**
 * Composes a filename from the directory path, the subjectType and the file extension
 * <p> Saves result in a static variable called fileName
 * @param subjectTypeForFile - integer value of the subject type
 */
void createFilename(int subjectTypeForFile);


/**
 * Gets the current stringToOutput
 * @return stringToOutput
 * @author Vanns Tacey
 */
char* getStringToOutput();


/**
 * Creates a composite output made of 3 independent strings
 * <p> File 1, userInput and file 2 </p>
 * @param *tmpBuffer - the buffer collected from the first file
 * @param *f - file pointer to second file
 * @author Vanns Tacey
 */
void getCompositeOutput(char* tmpBuffer, FILE *f);

/**
 * Gets an output from the output text file
 * <p> Saves output in a the static variable stringToOutput </p>
 * @param subjectType - the subject to get an output for
 * @return an integer status code
 * @author Vanns Tacey
 */
int getOutput(int subjectType);

#endif //CW2_OUTPUTFORMULATION_H
