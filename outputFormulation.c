//
// Created by Vanns Tacey on 17/03/2021.
//

#include "outputFormulation.h"
#include "global.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

static const char* dbDirectory = "txtFiles/outputFiles/";
static const char* fileExtension = ".txt";

// Static array that holds the string to output
static char stringToOutput[1024];

// Booleans used when creating an output
static int collectName = 0;
static int isInputName = 0;
static int compositeOutput = 0;

static char fileName[50];


void createFilename(int subjectTypeForFile) {
    snprintf(fileName, sizeof(fileName), "%s%d%s", dbDirectory, subjectTypeForFile, fileExtension);
}


double randDouble() {
    double div = RAND_MAX / 1;
    return 0 + (rand() / div);
}


char* getStringToOutput() {
    return stringToOutput;
}


int outputResult(const char* tmp_stringToOutput, int end, int usePassed) {
    char passedString[1024];
    if (usePassed) {
        strcpy(passedString, tmp_stringToOutput);
    } else {
        strcpy(passedString, stringToOutput);
    }

    if (strcmp(passedString, "\0") != 0) {
        printf("%s\n", passedString);
//        If there is a input following this output, prompt the user
        if (end != 1) {
            printf(": ");
        }
        return 1;
    }
    return 0;
}


void getCompositeOutput(char* tmpBuffer, FILE *f) {
    compositeOutput = 0;
    char buffer[1024];
    strcpy(buffer, tmpBuffer);

// Add a space to the first string to allow for a second string
    strncat(stringToOutput, " ", 1);

//    If a name is to be added, add first
    if (isInputName) {
        strncat(stringToOutput, userInput, sizeof(buffer));
        strncat(stringToOutput, "! ", 2);
    }

//    Add the final string to finish composite output
    fgets(buffer, sizeof(buffer), f);
    strncat(stringToOutput, buffer, sizeof(stringToOutput) - strlen(stringToOutput) - 1);
    fclose(f);
}


int getOutput(int subjectType) {
    int rtnValue = 1;

    if (collectName || subjectType == st_social_response) {
        isInputName = 1;
    } else {
        isInputName = 0;
    }

    collectName = 0;
    compositeOutput = 0;

    FILE *f_output = NULL;
    FILE *f_output_comp = NULL;

    createFilename(subjectType);
    f_output = fopen(fileName, "r");


    if (subjectType == st_media || subjectType == st_social) {
        collectName = 1;
    }
//    Switch statement for more unique cases (mainly composite outputs)
    switch (subjectType) {
        case 88: {
            createFilename(st_social_followOn);
            f_output_comp = fopen(fileName, "r");
            compositeOutput = 1;
            break;
        }
        case 6: {
            createFilename(st_followOn_questions);
            f_output_comp = fopen(fileName, "r");
            compositeOutput = 1;
            break;
        }
        case 97: {
            createFilename(st_followOn_questions);
            f_output_comp = fopen(fileName, "r");
            compositeOutput = 1;
            break;
        }
        case 98: {
            printf("That sounds interesting, but I don't quite understand\n");
            break;
        }
        case 99: {
            if (previousSubject == st_social_response) {
                fclose(f_output);

                createFilename(st_hobbies);
                f_output = fopen(fileName, "r");

                createFilename(st_goodbye);
                f_output_comp = fopen(fileName, "r");

                compositeOutput = 1;
            } else {
                createFilename(st_goodbye);
                f_output = fopen(fileName, "r");
            }
            break;
        }
    }

    if (collectName) {
        rtnValue = 2;
    }

//    If a file was selected, get a string from that file
    if (f_output) {
        int lineNo = 0;
        char buffer[1024];
//        Used for random number generation
        srand(time(NULL));

        while (fgets(buffer, sizeof(buffer), f_output)) {
            lineNo++;
            double randVal = randDouble();
            if (randVal < 1.0 / lineNo) {
                strcpy(stringToOutput, buffer);
            }
        }

        fclose(f_output);

        if (compositeOutput) {
            getCompositeOutput(buffer, f_output_comp);
            fclose(f_output_comp);
        }

        stringToOutput[strlen(stringToOutput)-1] = '\0';
    }


    return rtnValue;
}

