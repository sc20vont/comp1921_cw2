//
// Created by Vanns Tacey on 17/03/2021.
//
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "inputProcessing.h"
#include "global.h"

const char* phraseDB = "txtFiles/phrase_db.txt";
const char* wordDB = "txtFiles/word_db.txt";

// Variables used throughout module
char buffer[1030];
char bufferToUse[1024];

// Boolean value storing the certainty of response
int unsure = 0;

// *********************************************************
// Following functions used for processing input once it has been checked
// *********************************************************
int cleanString() {
    int finalPipe = -1;
    char subjectType[3];
    char* tmpBuffer = buffer;

    memset(bufferToUse, '\0', sizeof(bufferToUse));
//    Loops through and finds the final pipe character
    for (int i = 0; i < strlen(buffer); i++) {
        if (buffer[i] == '|') {
            finalPipe = i;
        }
    }

//    Storing subjectType that was found in the buffer
    strncpy(subjectType, &tmpBuffer[finalPipe+1], sizeof(subjectType));
//    Storing the new buffer with the int value removed
    strncpy(bufferToUse, buffer, finalPipe);
    snprintf(bufferToUse, sizeof(bufferToUse), "%s", bufferToUse);

    return atol(subjectType);
}


int searchPhraseDB(int rtnType) {
    FILE *f_phrase = fopen(phraseDB, "r");

    while (fgets(buffer, sizeof(buffer), f_phrase) != NULL) {
//        Removing the subject int at the end of line
        int subjectType = cleanString();

        switch (rtnType) {
            case 0: {
                if (strcmp(bufferToUse, userInput) == 0) {
                    fclose(f_phrase);
                    return subjectType;
                }
                break;
            }
            case 1: {
                if (subjectType > maxSubject && subjectType < 90) {
                    maxSubject = subjectType;
                }
                break;
            }
        }
    }

    fclose(f_phrase);
    return 0;
}


int searchWordDB() {
    int rtnVal = 0;
    FILE *f_wordDB = fopen(wordDB, "r");

    int counter = 0;
    int maxOccurrence = 1;
    int subjectTypeToKeep = 0;
    char bufferToKeep[1024];
    int confused = 0;

    while (fgets(buffer, sizeof(buffer), f_wordDB) != NULL) {
        counter = 0;
        int subjectType = cleanString();

//        Split string into tokens to count word occurrences
        const char delim[2] = "|";
        char *token = strtok(bufferToUse, delim);

        while(token != NULL) {
//            Check word occurrences
            for (int i = 0; i < numberOfWords; ++i) {
                if (strcmp(token, userInputSplit[i]) == 0) {
                    counter++;
                }
            }

            token = strtok(NULL, delim);
        }

//        Found a new 'closest' match in DB
        if (counter > maxOccurrence) {
            confused = 0;
            unsure = 0;
            maxOccurrence = counter;
            strcpy(bufferToKeep, bufferToUse);
            subjectTypeToKeep = subjectType;
        }

//        Have multiple occurrences in DB so response is not as clear
        if (counter == maxOccurrence) {
//            Definitely do not understand
            if (subjectType != subjectTypeToKeep) {
                confused = 1;
//            Possibly understand but unsure
            } else {
                unsure = 1;
            }
        }
    }
    fclose(f_wordDB);

    if (confused) {
        return 0;
    }

    if (subjectTypeToKeep > 0) {
        rtnVal = subjectTypeToKeep;
    }
    return rtnVal;
}


int addToWordDB(FILE *f, int subjectType) {
    if (f) {
        char string[45];

        for (int i = 0; i < numberOfWords_l; i++) {
            strcpy(string, userInputToLearnSplit[i]);

//            If we are on the final word, add a \n instead of a |
            if (i + 1 == numberOfWords_l) {
                snprintf(string, sizeof(string), "%s|%d\n", string, subjectType);
            } else {
                snprintf(string, sizeof(string), "%s|", string);
            }
            fprintf(f, "%s", string);
        }
        return 1;
    }
    return 0;
}


int addToDBs(int subjectType) {
//    Copying global string and making sure it ends
    char stringToAdd[1024];
    strcpy(stringToAdd, userInputToLearn);
    snprintf(stringToAdd, sizeof(stringToAdd), "%s|%d\n", stringToAdd, subjectType);

    FILE *f_phraseDB = fopen(phraseDB, "a");
    fprintf(f_phraseDB, "%s", stringToAdd);
    fclose(f_phraseDB);

    FILE *f_wordDB = fopen(wordDB, "a");
    int statusAddToWordDB = addToWordDB(f_wordDB, subjectType);
    fclose(f_wordDB);

    if (statusAddToWordDB == 1) {
        return 1;
    } else {
        return 0;
    }
}

// *********************************************************
// Following functions used for checking input is acceptable
// *********************************************************
int hasVowel(int wordToCheck) {
    char vowels[5] = {'a', 'e' , 'i', 'o', 'u'};

//    Loop through the word
    for (int i = 0; i < strlen(userInputSplit[wordToCheck]); i++) {
//        For each character in word, loop and check if its a vowel
        for (int j = 0; j < 5; j++) {
            if (userInputSplit[wordToCheck][i] == vowels[j]){
                return 1;
            }
        }
    }

    return 0;
}


int hasConsonant(int wordToCheck) {
    char consonants[21] = {'b', 'c' , 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};

//    Loop through the word
    for (int i = 0; i < strlen(userInputSplit[wordToCheck]); i++) {
//        For each character in word, loop and check if its a consonant
        for (int j = 0; j < 21; j++) {
            if (userInputSplit[wordToCheck][i] == consonants[j]){
                return 1;
            }
        }
    }

    return 0;
}


int hasNoNumbers(int wordToCheck) {
    char numbers[10] = {'1', '2' , '3', '4', '5', '6', '7', '8', '9', '0'};

//    Loop through the word
    for (int i = 0; i < strlen(userInputSplit[wordToCheck]); i++) {
//        For each character in word, loop and check if its a number
        for (int j = 0; j < 10; j++) {
            if (userInputSplit[wordToCheck][i] == numbers[j]){
                return 0;
            }
        }
    }

    return 1;
}


int hasDifferentLetters(int wordToCheck) {
    int repeatedLetters = 1;

//    Loop through the word
    for (int i = 0; i < strlen(userInputSplit[wordToCheck]); i++) {
        if (userInputSplit[wordToCheck][i] == userInputSplit[wordToCheck][i+1]) {
            repeatedLetters++;
        } else {
            repeatedLetters = 1;
        }

//        For example heyyy is not an acceptable word
        if (repeatedLetters >= 3) {
            return 0;
        }
    }

    return 1;
}


int isWord(int wordToCheck) {
//    Assuming true unless proven otherwise
    int hasConsonant_status = 1;
    int hasDifferentLetters_status = 1;
    int hasVowel_status = 1;
    int hasNoNumbers_status = 1;

    int lenOfWordToCheck = strlen(userInputSplit[wordToCheck]);

    switch (lenOfWordToCheck) {
//        If word is a single letter
        case 1: {
            hasVowel_status = hasVowel(wordToCheck);
            hasNoNumbers_status = hasNoNumbers(wordToCheck);
            break;
        }
//        If word has 2 letters
        case 2: {
            hasConsonant_status = hasConsonant(wordToCheck);
            hasNoNumbers_status = hasNoNumbers(wordToCheck);
            break;
        }
//        If word has > 2 letters
        default: {
            hasVowel_status = hasVowel(wordToCheck);
            hasNoNumbers_status = hasNoNumbers(wordToCheck);
            hasConsonant_status = hasConsonant(wordToCheck);
            hasDifferentLetters_status = hasDifferentLetters(wordToCheck);
        }
    }

    if (hasVowel_status && hasConsonant_status && hasDifferentLetters_status && hasNoNumbers_status) {
        return 1;
    } else {
        return 0;
    }
}
