//
// Created by Vanns Tacey on 10/04/2021.
//

#ifndef CW2_LEARNINPUT_H
#define CW2_LEARNINPUT_H


/**
 * Categorises the input to attempt to learn the subject type
 * <p> Assumes that the input has been categorised as a hobby</p>
 * @return an integer status code
 * @author Vanns Tacey
 */
int categoriseHobby();

/**
 * Categorises the input to attempt to learn the subject type
 * <p> Asks the user what category the input falls into.
 * If the input cannot be categorised, create a new file and new subject type for the input</p>
 * @return an integer status code
 * @author Vanns Tacey
 */
int categoriseInput();

/**
 * Begins the process to learn the input from the user
 * @return an integer status code
 * @author Vanns Tacey
 */
int learnFromUser();

#endif //CW2_LEARNINPUT_H
