//
// Created by Vanns Tacey on 10/04/2021.
//

#include "learnInput.h"
#include "global.h"
#include "inputCollection.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


static const char* notSensible = "That was not a sensible input :(";
static const char* describeInput = "Could you categorise what you said using one word?";
static const char* stopLearning = "If at anytime you wish to stop me from learning this input, type 'q'";

int addUnknownInput() {
    int rtnVal = 0;

    printf("%s\n", stopLearning);
    outputResult(describeInput, 0, 1);
    const char* customCategory = collectUserInput();

    if (strcmp(customCategory, "q") != 0) {
        char stringToPass[1024];

        snprintf(stringToPass, sizeof(stringToPass), "%s%s%s", "And what would an ideal response be to '", userInputToLearn, "'?");
        outputResult(stringToPass, 0, 1);
        const char* tmp_customResponse = collectUserInput();

        char customResponse[1024];
        strcpy(customResponse, tmp_customResponse);

        if (strcmp(customCategory, "q") != 0) {
            snprintf(stringToPass, sizeof(stringToPass), "%s%s%s%s%s%s%s", "So you want me to learn '", userInputToLearn, "' is of type '", customCategory, "' and a reasonable response would be '", customResponse, "'? [y/n]");
            outputResult(stringToPass, 0, 1);
            int finalCheck = getYesNoAnswer();

            if (finalCheck) {
//                Create files and add to DB
                searchPhraseDB(1);
                maxSubject++;

                char fileName[50];
                snprintf(fileName, sizeof(fileName), "%s%s%d%s", "txtFiles/outputFiles/", customCategory, maxSubject, ".txt");
                strcat(customResponse, "\n");

                FILE *fileToCreate = fopen(fileName, "w");
                fwrite(customResponse, 1024, 1, fileToCreate);
                fclose(fileToCreate);

                addToDBs(maxSubject);

                rtnVal = 1;
            }
        }
    }
    return rtnVal;
}


int categoriseHobby() {
    int rtnVal;
//    Text menu for user to categorise hobby
    printf("Could you fit '%s' into any of these types of hobby?\n", userInputToLearn);
    printf("1 - Social\n");
    printf("2 - Media\n");
    printf("3 - Game\n");
    printf("4 - Sport\n");
    printf("5 - Reading\n");
    printf("6 - Instrument\n");
    printf("7 - Music\n");
    printf("8 - Other\n");
    printf("Please enter the integer value for the category\n");
    int hobbySubmitted = atoi(collectUserInput());

//    Adds the input to the DBs based on the category submitted
    switch (hobbySubmitted) {
        case 1: {
            rtnVal = st_social;
            break;
        }
        case 2: {
            rtnVal = st_media;
            break;
        }
        case 3: {
            rtnVal = st_game;
            break;
        }
        case 4: {
            rtnVal = st_sport;
            break;
        }
        case 5: {
            rtnVal = st_reading;
            break;
        }
        case 6: {
            rtnVal = st_instrument;
            break;
        }
        case 7: {
            rtnVal = st_music;
            break;
        }
        case 8: {
            rtnVal = st_hobbies;
            break;
        }
        default: {
            printf("%s", notSensible);
            categoriseHobby();
        }
    }

    return rtnVal;
}


int categoriseInput() {
    int rtnVal;
//    Text menu for user to categorise input
//  TODO: add more categories to choose from e.g mood improvment
    printf("Could you fit '%s' into any of these categories?\n", userInputToLearn);
    printf("1 - Greeting\n");
    printf("2 - Complement\n");
    printf("3 - Insult\n");
    printf("4 - A type of hobby\n");
    printf("5 - Other\n");
    printf("Please enter the integer value for the category\n");
    int categorySubmitted = atoi(collectUserInput());

//    Adds the input to the DBs based on the category submitted
    switch (categorySubmitted) {
        case 1: {
            addToDBs(1);
            rtnVal = 1;
            break;
        }
        case 2: {
            addToDBs(10);
            rtnVal = 1;
            break;
        }
        case 3: {
            addToDBs(11);
            rtnVal = 1;
            break;
        }
        case 4: {
            int subjectTypeToAdd = categoriseHobby();
            addToDBs(subjectTypeToAdd);
            rtnVal = 1;
            break;
        }
        case 5: {
            rtnVal = addUnknownInput();
            break;
        }
        default: {
            printf("%s", notSensible);
            rtnVal = 0;
        }
    }

    return rtnVal;
}


int learnFromUser() {
    int categoriseInput_status = categoriseInput();
    if (!categoriseInput_status) {
        learnFromUser();
    } else {
        return categoriseInput_status;
    }
}