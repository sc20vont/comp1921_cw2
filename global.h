//
// Created by Vanns Tacey on 17/03/2021.
//

#ifndef CW2_GLOBAL_H
#define CW2_GLOBAL_H

#include <stdio.h>

// SubjectType Variables
extern int st_greeting;
extern int st_good_well_being;
extern int st_bad_well_being;
extern int st_hobbies;
extern int st_mood_improvement;
extern int st_media;
extern int st_social;
extern int st_complement;
extern int st_insult;
extern int st_game;
extern int st_sport;
extern int st_reading;
extern int st_instrument;
extern int st_music;

extern int st_bad_well_being_reason;
extern int st_social_response;
extern int st_social_followOn;
extern int st_media_followOn;
extern int st_followOn_questions;
extern int st_goodbye;



// Constant strings to use throughout program
extern const char* worthLearning;
extern const char* notLearning;
extern const char* restarting;
extern const char* notEnglish;
extern const char* yesNoFailure;


// Global Variables to store information about the users input
extern char userInput[1024];
extern int lenOfUserInput;

extern char userInputSplit[1024][45];
extern int numberOfWords;

// Global Variables to store information about the users input to learn
//      (Different as only want to learn inputs that are unknown and relevant)
extern char userInputToLearn[1024];
extern int lenOfUserInputToLearn;

extern char userInputToLearnSplit[1024][45];
extern int numberOfWords_l;

extern char userInputPipes[1024];

// Boolean storing whether or not to learn the current input
extern int learnWord;

// Stores the subject of the last output
extern int previousSubject;

// The highest number used to represent a subject
extern int maxSubject;

/**
 * Adds current userInputToLearn to the phrase and word databases
 * @param subjectType - an integer code that signifies the subject of the input
 * @return an integer status code
 * @author Vanns Tacey
 */
int addToDBs(int subjectType);

/**
 * Adds current userInputToLearn to the word database
 * <p> Adds | between words for word DB </p>
 * @param f - a pointer to the file to write in
 * @param subjectType - an integer code that signifies the subject of the input
 * @return an integer status code
 * @author Vanns Tacey
 */
int addToWordDB(FILE *f, int subjectType);


int getYesNoAnswer();


/**
 * Removes integer subject type from string in file
 * @return the integer removed from the string
 * @author Vanns Tacey
 */
int cleanString();


/**
 * Searches the phrase text file for the string inputted
 * @param rtnType - 0 will search for the userInput, 1 will search for the highest subject value
 * @return an integer corresponding to the subject of the input found
 * @author Vanns Tacey
 */
int searchPhraseDB(int rtnType);


/**
 * Outputs a given string
 * @param end - a boolean integer that stores whether or not the output is the end of the program
 * @param tmp_stringToOutput - string to be copied to the static variable and be outputted
 * @param usePassed - a boolean integer that determines whether or not to use the passes string
 * @return an integer status code
 * @author Vanns Tacey
 */
int outputResult(const char* tmp_stringToOutput, int end, int usePassed);

#endif //CW2_GLOBAL_H
