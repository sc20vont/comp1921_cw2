#include <stdio.h>
#include <string.h>

#include "inputCollection.h"
#include "inputProcessing.h"
#include "outputFormulation.h"
#include "global.h"
#include "learnInput.h"
#include "interface.h"

char userInput[1024];
int lenOfUserInput = 0;
char userInputSplit[1024][45];
int numberOfWords = 0;

char userInputToLearn[1024];
int lenOfUserInputToLearn = 0;
char userInputToLearnSplit[1024][45];
int numberOfWords_l = 0;
char userInputPipes[1024];

int learnWord = 1;
int getName = 0;
int previousSubject;
int maxSubject = 13;

int st_greeting = 1;
int st_good_well_being = 2;
int st_bad_well_being = 3;
int st_ok_well_being = 4;
int st_hobbies = 5;
int st_mood_improvement = 6;
int st_media = 7;
int st_social = 8;
int st_complement = 9;
int st_insult = 10;
int st_game = 11;
int st_sport = 12;
int st_reading = 13;
int st_instrument = 14;
int st_music = 15;

int st_bad_well_being_reason = 83;
int st_social_response = 88;
int st_social_followOn = 98;
int st_media_followOn = 97;
int st_followOn_questions = 100;
int st_goodbye = 101;

const char* worthLearning = "Do you want me to learn this input? [y/n]";
const char* notLearning = "Okay, I will not learn from this input";
const char* restarting = "I will now restart...";
const char* notEnglish = "Sorry, I don't believe you entered standard English text";
const char* yesNoFailure = "Sorry, I don't believe you entered y or n";


void welcomeUser() {
    printf("***********************************\n");
    printf("%s\n", startUpGreeting);
    printf("***********************************\n");
    printf("\n");
    outputResult(promptUser, 0, 1);
}


void getUserInput() {
    const char* tmp_userInput = collectUserInput();
    strcpy(userInput, tmp_userInput);
    lenOfUserInput = strlen(userInput);
    numberOfWords = splitInputtedString(0);

    if (learnWord == 1) {
        strcpy(userInputToLearn, userInput);
        lenOfUserInputToLearn = strlen(userInputToLearn);
        numberOfWords_l = splitInputtedString(1);
    }

    for (int i = 0; i < lenOfUserInput; i++) {
        if (userInput[i] == ' ') {
            userInputPipes[i] = '|';
        } else {
            userInputPipes[i] = userInput[i];
        }
    }
}


int getYesNoAnswer() {
    const char* yesNoResponse = collectUserInput();
    if (strcmp(yesNoResponse, "y") == 0) {
        return 1;
    } else {
        if (strcmp(yesNoResponse, "n") == 0) {
        return 0;
        } else {
            return -1;
        }
    }
}


int learnInput() {
    int rtnVal = 0;
    outputResult(worthLearning, 0, 1);
    int response = getYesNoAnswer();
    switch (response) {
        case 1: {
            rtnVal = 1;
            break;
        }
        case 0: {
           printf("%s\n", notLearning);
           outputResult(restarting, 1, 1);
           break;
        }
        case -1: {
            printf("%s\n", yesNoFailure);
            learnInput();
        }
    }

    return rtnVal;
}


int checkInput() {
    if (strcmp(userInput, "q") == 0) {
        return 1;
    } else {
        int isWord_status;

        for (int i = 0; i < numberOfWords; i++) {
            isWord_status = isWord(i);

            if (isWord_status == 0) {
                return 0;
            }
        }
        return 1;
    }
}


int getApprovalFromUser() {
    int rtnVal;

    char* chatbotResponse = strdup(getStringToOutput());
    char reasonableResponse[1024];
    snprintf(reasonableResponse, sizeof(reasonableResponse), "%s%s%s", "I did not completely understand the input, would '", chatbotResponse, "' be a resonable response? [y/n]");
    outputResult(reasonableResponse, 0, 1);
    rtnVal = getYesNoAnswer();

    if (rtnVal == -1) {
        getApprovalFromUser();
    }

    return rtnVal;
}


int programLoop() {
    int rtnValue = 1;
    int subjectType;
    learnWord = 1;

    getUserInput();

    int checkInput_status = checkInput();

    if (checkInput_status) {
        if (getName) {
            if (previousSubject == st_media) {
                subjectType = st_media_followOn;
            }
            if (previousSubject == st_social) {
                subjectType = st_social_response;
            }

        } else {
            if (strcmp(userInput, "q") == 0 || previousSubject == st_social_response) {
//                TODO: ask if there is anything else they wish to say after st_social_response
                subjectType = st_goodbye;
            } else {
                subjectType = searchPhraseDB(0);

                if (subjectType <= 0) {
                    subjectType = searchWordDB();
//                    Do not understand input
                    if (subjectType == 0) {
                        subjectType = -1;
                        learnWord = 0;
                        rtnValue = 2;
                    }
                }
            }
        }

        if (rtnValue != 2) {
            //    Formulate output
            int getOutputStatus = getOutput(subjectType);
            if (getOutputStatus == 2) {
                getName = 1;
            } else {
                getName = 0;
            }

            if (getOutputStatus != 0) {
                if (subjectType == st_goodbye) {
                    outputResult("", 1, 0);
                    rtnValue = 0;
                } else {

//                    Possibly understand but are not quite sure
                    if (unsure) {
                        learnWord = 0;
                        rtnValue = 3;
                    } else {
                        outputResult("", 0, 0);
                        rtnValue = 1;
                    }
                }
            }
        }

        previousSubject = subjectType;
    } else {
        rtnValue = -1;
    }


    return rtnValue;
}


int main() {
    welcomeUser();

    int status = 1;
    while (status != 0) {
        status = programLoop();

        switch (status) {
//            User input is not recognisable
            case -1: {
                printf("%s\n", notEnglish);
                printf("%s\n", restarting);
                main();
            }
//            User input is not understood, so ask the user if they want the input to be learnt
            case 2: {
                learnWord = learnInput();
                if (learnWord) {
                    learnFromUser();
                    printf("I have successfully learnt '%s'\n", userInputToLearn);
                    printf("%s\n", restarting);
                    main();
                } else {
                    main();
                }
                break;
            }
//            User input is possibly understood, but not sure so check with user
            case 3: {
                int getApproval_status = getApprovalFromUser();
                if (getApproval_status) {
//                    Add input to DBs so it can definitely be understood next time
                    addToDBs(previousSubject);
                    outputResult("", 0, 0);
                } else {
//                    Learn the word as the do not understand it
                    learnFromUser();
                }
            }
        }
    }

    return 0;
}
